-*- coding: utf-8 -*-
import re
import unicodedata
  
text = u'João'
nfkd = unicodedata.normalize('NFKD', text)
text =  u"".join([c for c in nfkd if not unicodedata.combining(c)])
  
ACCENT_STRINGS = u'ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿ'
NO_ACCENT_STRINGS = u'SOZsozYYuAAAAAAACEEEEEIIIIIDNOOOOOOUUUUYsaaaaaaaceeeeeiiiiionoooooouuuuyy'
  
accentStrings = list(ACCENT_STRINGS)
noAccentStrings = list(NO_ACCENT_STRINGS.lower())
regex = {}
key = 0
for value in noAccentStrings:
    if value in regex:
        regex[value] += accentStrings[key]
    else:
        regex[value] = value
    key += 1
  
for rg in regex:
    regexText1 = r'%s' % rg
    regexText2 = r'[%s]' % regex[rg]
    text = re.sub(regexText1,regexText2, text, re.IGNORECASE)
  
print text